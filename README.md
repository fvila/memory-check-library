Memcheck library
================

Introduction
------------

This library contains a function memcheck which returns the total and
available amount of RAM memory. It is intended to be cross-platform,
for now it supports windows and linux.

Usage
-----

Here comes an example program which uses the library:

	#include <memcheck.h>
	#include <stdio.h>

	int main (int argc, char** argv)
	{
		unsigned long long int total, avail;

		total = 0;
		avail = 0;

		memcheck (total, avail);

		printf ("Total: %ull\n"\
				"Avail: %ull\n", total, avail);

		return 0;
	}
