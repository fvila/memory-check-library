INCLUDE_DIRECTORIES (./include)

IF (WIN32)
    SET (SOURCES
        internal/memcheck_win.c
        include/memcheck.h
    )
ELSEIF(UNIX)
    SET (SOURCES
        internal/memcheck_lin.c
        include/memcheck.h
    )
ELSE()
    MESSAGE( FATAL_ERROR "This operating system is not supported by now")
ENDIF()

ADD_LIBRARY(memcheck ${SOURCES})
