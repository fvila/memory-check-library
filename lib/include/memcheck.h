#ifndef __MEMCHECK_H
#define __MEMCHECK_H

void memcheck (unsigned long long *totalPhys,
			   unsigned long long *availPhys);

#endif // __MEMCHECK_H
