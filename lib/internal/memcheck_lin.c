#include <sys/sysinfo.h>
#include <memcheck.h>

void memcheck (unsigned long long *totalPhys, 
			   unsigned long long *availPhys)
{
	struct sysinfo info;
	sysinfo(&info);

	*totalPhys = info.totalram;
	*availPhys = info.freeram;
}

