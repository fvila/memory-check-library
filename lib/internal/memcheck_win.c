#if defined __MINGW32__
#define WINVER 0x0501
#define _WIN32_WINNT 0x0501

#include <w32api.h>
#endif

#include <windows.h>
#include <memcheck.h>

void memcheck(unsigned long long *totalPhys,
              unsigned long long *availPhys)
{
	MEMORYSTATUSEX memory_status;
	
	ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
	memory_status.dwLength = sizeof(MEMORYSTATUSEX);

	if (GlobalMemoryStatusEx(&memory_status))
	{
	    *totalPhys = memory_status.ullTotalPhys;
	    *availPhys = memory_status.ullAvailPhys;
	}
}

