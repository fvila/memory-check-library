#include <memcheck.h>
#include <stdio.h>

int main ()
{
	unsigned long long total = 0,
				  	   avail = 0;

	memcheck (&total, &avail);

	printf ("RAM %lld/%lld\n", avail, total);

	return 0;
}
